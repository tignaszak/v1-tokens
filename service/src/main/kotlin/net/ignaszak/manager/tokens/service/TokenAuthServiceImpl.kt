package net.ignaszak.manager.tokens.service

import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.jwt.exception.JwtException
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tokens.entity.SecurityEntity
import net.ignaszak.manager.tokens.entity.TokenEntity
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository
import net.ignaszak.manager.tokens.service.error.TokenError.*
import net.ignaszak.manager.tokens.service.error.exception.TokenException
import net.ignaszak.manager.tokens.servicecontract.TokenAuthService
import org.springframework.stereotype.Service
import java.time.temporal.ChronoUnit.MILLIS
import org.apache.logging.log4j.LogManager

@Service
class TokenAuthServiceImpl(
        private val securityRepository: SecurityRepository,
        private val tokenRepository: TokenRepository,
        private val jwtService: JwtService,
        private val dateTimeProvider: IDateTimeProvider
): TokenAuthService {

    companion object {
        private val log = LogManager.getLogger(TokenAuthServiceImpl)
    }

    override fun validate(jwt: String) {
        var success = false

        if (jwt.isNotBlank()) {
            tokenRepository.findByJwt(jwt)
                .ifPresent {
                    val now = dateTimeProvider.getNow()
                    val expirationDateTime = it
                        .creationDate!!
                        .plus(it.timeToLiveMs, MILLIS)

                    if (now.isBefore(expirationDateTime)) {
                        val securityEntity = securityRepository.findByPublicId(it.securityPublicId).orElseThrow {
                            throw TokenException(TOK_INACTIVE)
                        }
                        if (!securityEntity.active) {
                            throw TokenException(TOK_INACTIVE)
                        }

                        it.loginDate = now

                        tokenRepository.save(it)
                        securityRepository.save(securityEntity)

                        success = true
                    }
                }
        }

        if (!success) throw TokenException(TOK_INVALID_TOKEN)
    }

    override fun register(jwt: String, userPublicId: String) = securityRepository
        .findByUserPublicId(userPublicId)
        .ifPresentOrElse({
            if (it.findTokenByJwt(jwt).isPresent) {
                throw TokenException(TOK_TOKEN_EXISTS)
            } else {
                addTokenToSecurity(jwt, userPublicId, it)
            }
        }, {
            val securityEntity = SecurityEntity(userPublicId = userPublicId, creationDate = dateTimeProvider.getNow())
            addTokenToSecurity(jwt, userPublicId, securityEntity)
        })

    private fun addTokenToSecurity(jwt: String, userPublicId: String, securityEntity: SecurityEntity) = try {
        val jwtTO = jwtService.getJwtTO(jwt)

        if (userPublicId != jwtTO.jwtUserTO.publicId) {
            throw TokenException(TOK_INVALID_TOKEN)
        } else {
            val token = getTokenEntity(jwt, securityEntity.publicId)
            tokenRepository.save(token)

            securityEntity.addToken(token)
            securityRepository.save(securityEntity)
        }
    } catch (e: JwtException) {
        log.error(e)

        throw TokenException(TOK_INVALID_TOKEN)
    }

    override fun activate(userPublicId: String) = securityRepository
        .findByUserPublicId(userPublicId)
        .ifPresentOrElse({
            it.active = true
            securityRepository.save(it)
        }, {
            throw TokenException(TOK_INVALID_TOKEN)
        })

    private fun getTokenEntity(jwt: String, securityPublicId: String) = TokenEntity(
        jwt = jwt,
        timeToLiveMs = jwtService.getJwtTimeToLiveMs(),
        securityPublicId = securityPublicId,
        creationDate = dateTimeProvider.getNow()
    )
}
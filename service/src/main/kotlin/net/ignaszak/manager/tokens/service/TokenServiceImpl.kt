package net.ignaszak.manager.tokens.service

import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository
import net.ignaszak.manager.tokens.servicecontract.TokenService
import org.springframework.stereotype.Service
import java.time.Period
import java.util.stream.Collectors

@Service
class TokenServiceImpl(private val securityRepository: SecurityRepository,
                       private val tokenRepository: TokenRepository,
                       private val dateTimeProvider: IDateTimeProvider) : TokenService {
    override fun delete(userPublicId: String, token: String) = securityRepository
        .findByUserPublicId(userPublicId)
        .ifPresent {
            it.findTokenByJwt(token).ifPresent(tokenRepository::delete)

            it.tokens = it.tokens
                .stream()
                .filter {te ->
                    te.jwt != token
                }
                .collect(Collectors.toSet())

            securityRepository.save(it)
        }

    override fun deleteAll(userPublicId: String) = securityRepository
        .findByUserPublicId(userPublicId)
        .ifPresent {
            it.tokens.forEach(tokenRepository::delete)
            it.tokens.clear()
            securityRepository.save(it)
        }

    override fun deleteInactiveAfterPeriod(period: Period) {
        val now = dateTimeProvider.getNow()
        securityRepository
            .findAllByActiveIsFalse()
            .stream()
            .filter {
                it.creationDate.plus(period).isBefore(now)
            }
            .forEach {
                it.tokens.forEach(tokenRepository::delete)
                securityRepository.delete(it)
            }
    }
}
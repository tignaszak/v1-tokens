package net.ignaszak.manager.tokens.service.error.exception

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.tokens.service.error.TokenError
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(BAD_REQUEST)
class TokenException(tokenError: TokenError) : ErrorException(tokenError)
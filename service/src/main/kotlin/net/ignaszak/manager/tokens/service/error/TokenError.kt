package net.ignaszak.manager.tokens.service.error

import net.ignaszak.manager.commons.error.Error

enum class TokenError(override val code: String, override val message: String) : Error {
    TOK_INVALID_TOKEN("TOK-1", "Invalid authentication token!"),
    TOK_TOKEN_EXISTS("TOK-2", "Token already exists!"),
    TOK_INACTIVE("TOK-3", "Inactive token!");

    override fun toString(): String {
        return super.getString()
    }
}
package net.ignaszak.manager.tokens.service

import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tokens.entity.SecurityEntity
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository
import net.ignaszak.manager.tokens.service.error.exception.TokenException
import net.ignaszak.manager.tokens.servicecontract.TokenAuthService
import spock.lang.Ignore
import spock.lang.Specification

import java.time.LocalDateTime

import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyJwtTO
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.tokens.service.TestHelper.NOW
import static net.ignaszak.manager.tokens.service.TestHelper.anySecurityEntity
import static net.ignaszak.manager.tokens.service.TestHelper.anyTokenEntity
import static net.ignaszak.manager.tokens.service.TestHelper.jwtTokenWithNotMatchingUserPublicId
import static net.ignaszak.manager.tokens.service.error.TokenError.TOK_INACTIVE
import static net.ignaszak.manager.tokens.service.error.TokenError.TOK_INVALID_TOKEN
import static net.ignaszak.manager.tokens.service.error.TokenError.TOK_TOKEN_EXISTS

class TokenAuthServiceImplSpec extends Specification {

    private SecurityRepository securityRepositoryMock = Mock()
    private TokenRepository tokenRepositoryMock = Mock()
    private JwtService jwtServiceMock = Mock()

    private IDateTimeProvider dateTimeProviderMock = new IDateTimeProvider() {
        @Override
        LocalDateTime getNow() {
            return LocalDateTime.now()
        }
    }

    private TokenAuthService subject = new TokenAuthServiceImpl(securityRepositoryMock, tokenRepositoryMock, jwtServiceMock, dateTimeProviderMock)

    def  "update token login date when token exists and is not expired"() {
        given:
        def token = anyToken()
        def tokenEntity = anyTokenEntity()
        1 * tokenRepositoryMock.findByJwt(token) >> Optional.of(tokenEntity)
        1 * securityRepositoryMock.findByPublicId(tokenEntity.securityPublicId) >> Optional.of(anySecurityEntity())

        when:
        subject.validate(token)

        then:
        tokenEntity.loginDate.isAfter(NOW)
        1 * tokenRepositoryMock.save(tokenEntity)
    }

    def "throw exception when token exists and is expired"() {
        given:
        def token = anyToken()
        def tokenEntity = anyTokenEntity(0L)
        1 * tokenRepositoryMock.findByJwt(token) >> Optional.of(tokenEntity)

        when:
        subject.validate(token)

        then:
        TokenException e = thrown()
        e.message == TOK_INVALID_TOKEN.toString()
        0 * tokenRepositoryMock.save(tokenEntity)
    }

    def "throw exception when token not exists"() {
        given:
        def token = anyToken()
        1 * tokenRepositoryMock.findByJwt(token) >> Optional.empty()

        when:
        subject.validate(token)

        then:
        TokenException e = thrown()
        e.message == TOK_INVALID_TOKEN.toString()
        0 * tokenRepositoryMock.save(any())
    }

    def "throw exception when token is inactive"() {
        given:
        def token = anyToken()
        1 * tokenRepositoryMock.findByJwt(token) >> Optional.of(anyTokenEntity())
        1 * securityRepositoryMock.findByPublicId(anyTokenEntity().securityPublicId) >> Optional.of(anySecurityEntity(false))

        when:
        subject.validate(token)

        then:
        TokenException e = thrown()
        e.message == TOK_INACTIVE.toString()
        0 * tokenRepositoryMock.save(any())
    }

    def "throw exception when token entity has incorrect reference to security entity"() {
        given:
        def token = anyToken()
        1 * tokenRepositoryMock.findByJwt(token) >> Optional.of(anyTokenEntity())
        1 * securityRepositoryMock.findByPublicId(anyTokenEntity().securityPublicId) >> Optional.empty()

        when:
        subject.validate(token)

        then:
        TokenException e = thrown()
        e.message == TOK_INACTIVE.toString()
        0 * tokenRepositoryMock.save(any())
    }

    @Ignore("MAN-166")
    def "register new jwt token for no existing security"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.empty()
        1 * jwtServiceMock.getJwtTO(anyToken()) >> anyJwtTO()

        when:
        subject.register(anyToken(), ANY_PUBLIC_ID)

        then:
        1 * tokenRepositoryMock.save(_)
        1 * securityRepositoryMock.save({
            it instanceof SecurityEntity
            it.tokens.size() == 1
        })
    }

    def "throw exception if token does not exists and is invalid"() {
        given:
        securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.empty()
        jwtServiceMock.getJwtTO(anyToken()) >> jwtTokenWithNotMatchingUserPublicId()

        when:
        subject.register(anyToken(), ANY_PUBLIC_ID)

        then:
        TokenException e = thrown()
        e.message == TOK_INVALID_TOKEN.toString()
    }

    def "throw exception if token already exists while adding new jwt"() {
        given:
        securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.of(anySecurityEntity(false, Set.of(anyTokenEntity())))

        when:
        subject.register(anyToken(), ANY_PUBLIC_ID)

        then:
        TokenException e = thrown()
        e.message == TOK_TOKEN_EXISTS.toString()
    }

    def "activate token by user public id"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.of(anySecurityEntity(false))

        when:
        subject.activate(ANY_PUBLIC_ID)

        then:
        1 * securityRepositoryMock.save({
            it instanceof SecurityEntity
            it.active
        })
    }

    def "throw an exception while activation no existing security entry"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.empty()

        when:
        subject.activate(ANY_PUBLIC_ID)

        then:
        TokenException e = thrown()
        e.message == TOK_INVALID_TOKEN.toString()
    }
}

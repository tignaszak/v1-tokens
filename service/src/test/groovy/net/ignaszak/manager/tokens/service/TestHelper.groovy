package net.ignaszak.manager.tokens.service

import net.ignaszak.manager.commons.jwt.to.JwtTO
import net.ignaszak.manager.commons.jwt.to.JwtUserTO
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.tokens.entity.SecurityEntity
import net.ignaszak.manager.tokens.entity.TokenEntity
import net.ignaszak.manager.tokens.to.TokenTO

import java.time.LocalDateTime

import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyJwtTO
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID

final class TestHelper {

    static final LocalDateTime NOW = LocalDateTime.now()
    static final long TIME_TO_LIVE_MS = 2_592_000_000

    static SecurityEntity anySecurityEntity(boolean isActive = true, def tokens = new HashSet<>())
    {
        new SecurityEntity(anySecurityPublicId(), ANY_PUBLIC_ID, NOW, tokens, isActive)
    }

    static JwtTO jwtTokenWithNotMatchingUserPublicId() {
        def jwtUserTO = new JwtUserTO("other user public id", UserHelper.ANY_EMAIL)
        return new JwtTO(jwtUserTO, anyJwtTO().roles)
    }

    static TokenEntity anyTokenEntity(timeToLiveMs = TIME_TO_LIVE_MS) {
        new TokenEntity(anyTokenPublicId(), anyToken(), anySecurityPublicId(), NOW, NOW, timeToLiveMs)
    }

    static String anySecurityPublicId() {
        "da3da28b-ab50-4929-910c-afd750d2a453"
    }

    static String anyTokenPublicId() {
        "any public id"
    }

    static anyTokenTO(loginDate = NOW) {
        return new TokenTO(anyTokenPublicId(), anyToken(), ANY_PUBLIC_ID, NOW, loginDate, TIME_TO_LIVE_MS)
    }

    static boolean isUUID(String value) {
        try {
            UUID.fromString(value)
            return true
        } catch (Exception ignored) {
            return false
        }
    }

    private TestHelper() {}
}

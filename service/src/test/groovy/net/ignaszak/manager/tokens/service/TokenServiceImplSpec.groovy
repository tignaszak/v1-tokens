package net.ignaszak.manager.tokens.service

import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tokens.entity.SecurityEntity
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository
import net.ignaszak.manager.tokens.servicecontract.TokenService
import spock.lang.Specification

import java.time.LocalDateTime

import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.tokens.service.TestHelper.anySecurityEntity
import static net.ignaszak.manager.tokens.service.TestHelper.anyTokenEntity

class TokenServiceImplSpec extends Specification {

    private SecurityRepository securityRepositoryMock = Mock()
    private TokenRepository tokenRepositoryMock = Mock()

    private IDateTimeProvider dateTimeProvider = new IDateTimeProvider() {
        @Override
        LocalDateTime getNow() {
            LocalDateTime.now()
        }
    }

    private TokenService subject = new TokenServiceImpl(securityRepositoryMock, tokenRepositoryMock, dateTimeProvider)

    def "delete token if exists"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.of(anySecurityEntity(true, Set.of(anyTokenEntity())))

        when:
        subject.delete(ANY_PUBLIC_ID, anyToken())

        then:
        1 * tokenRepositoryMock.delete(_)
        1 * securityRepositoryMock.save(_)
    }

    def "do not delete no existing token"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.of(anySecurityEntity())

        when:
        subject.delete(ANY_PUBLIC_ID, anyToken())

        then:
        0 * tokenRepositoryMock.delete(_)
        1 * securityRepositoryMock.save(_)
    }

    def "do not delete token for no existing security"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.empty()

        when:
        subject.delete(ANY_PUBLIC_ID, anyToken())

        then:
        0 * tokenRepositoryMock.delete(_)
        0 * securityRepositoryMock.save(_)
    }

    def "delete all tokens"() {
        given:
        def anyUserPublicId = "user public id"
        def tokens = new HashSet<>()
        tokens.add(anyTokenEntity())
        1 * securityRepositoryMock.findByUserPublicId(anyUserPublicId) >> Optional.of(anySecurityEntity(true, tokens))

        when:
        subject.deleteAll(anyUserPublicId)

        then:
        1 * tokenRepositoryMock.delete(_)
        1 * securityRepositoryMock.save({
            it instanceof SecurityEntity
            it.tokens.size() == 0
        })
    }

    def "do not delete no existing tokens"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.of(anySecurityEntity())

        when:
        subject.deleteAll(ANY_PUBLIC_ID)

        then:
        0 * tokenRepositoryMock.delete(_)
        1 * securityRepositoryMock.save(_)
    }

    def "do not delete tokens for no existing security"() {
        given:
        1 * securityRepositoryMock.findByUserPublicId(ANY_PUBLIC_ID) >> Optional.empty()

        when:
        subject.deleteAll(ANY_PUBLIC_ID)

        then:
        0 * tokenRepositoryMock.delete(_)
        0 * securityRepositoryMock.save(_)
    }
}

package net.ignaszak.manager.tokens.repository

import net.ignaszak.manager.tokens.entity.SecurityEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SecurityRepository: CrudRepository<SecurityEntity, String> {
    fun findByUserPublicId(userPublicId: String): Optional<SecurityEntity>
    fun findByPublicId(publicId: String): Optional<SecurityEntity>
    fun findAllByActiveIsFalse(): Set<SecurityEntity>
}
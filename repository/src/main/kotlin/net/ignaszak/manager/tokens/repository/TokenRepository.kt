package net.ignaszak.manager.tokens.repository

import net.ignaszak.manager.tokens.entity.TokenEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TokenRepository : CrudRepository<TokenEntity, String> {
    fun findByJwt(jwt: String): Optional<TokenEntity>
}
package net.ignaszak.manager.tokens.entity

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.TimeToLive
import org.springframework.data.redis.core.index.Indexed
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit.MILLISECONDS

@RedisHash("TOKEN")
data class TokenEntity(
        @Id val publicId: String = UUID.randomUUID().toString(),
        @Indexed var jwt: String = "",
        var securityPublicId: String = "",
        val creationDate: LocalDateTime? = null,
        var loginDate: LocalDateTime? = null,
        @TimeToLive(unit = MILLISECONDS) var timeToLiveMs: Long = 0
): Serializable

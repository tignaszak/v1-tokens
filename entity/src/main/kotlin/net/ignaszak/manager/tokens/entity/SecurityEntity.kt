package net.ignaszak.manager.tokens.entity

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Reference
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.index.Indexed
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

@RedisHash("SECURITY")
data class SecurityEntity(
    @Id @Indexed val publicId: String = UUID.randomUUID().toString(),
    @Indexed val userPublicId: String,
    val creationDate: LocalDateTime,
    @Reference var tokens: MutableSet<TokenEntity> = mutableSetOf(),
    @Indexed var active: Boolean = false
) : Serializable {

    fun findTokenByJwt(jwt: String): Optional<TokenEntity> = tokens
        .stream()
        .filter {
            jwt == it.jwt
        }.findAny()

    fun addToken(token: TokenEntity) = tokens.add(token)
}

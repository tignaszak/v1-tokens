package net.ignaszak.manager.tokens.servicecontract

interface TokenAuthService {
    fun validate(jwt: String)
    fun register(jwt: String, userPublicId: String)
    fun activate(userPublicId: String)
}
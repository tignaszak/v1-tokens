package net.ignaszak.manager.tokens.servicecontract

import java.time.Period

interface TokenService {
    fun delete(userPublicId: String, token: String)
    fun deleteAll(userPublicId: String)
    fun deleteInactiveAfterPeriod(period: Period)
}
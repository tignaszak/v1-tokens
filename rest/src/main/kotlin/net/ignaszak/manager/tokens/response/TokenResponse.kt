package net.ignaszak.manager.tokens.response

import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

@Schema(title = "Token response")
data class TokenResponse(
        @Schema(description = "Token public id") val publicId: String,
        @Schema(description = "User public id") val userPublicId: String,
        @Schema(description = "Token creation date") val creationDate: LocalDateTime,
        @Schema(description = "User login date") val loginDate: LocalDateTime,
        @Schema(description = "Token expiration time (ms)") val timeToLiveMs: Long
)

package net.ignaszak.manager.tokens.integration

import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import net.ignaszak.manager.tokens.integration.builder.SecurityBuilder
import net.ignaszak.manager.tokens.integration.builder.TokenBuilder
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository
import net.ignaszak.manager.tokens.servicecontract.TokenService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.GenericContainer
import spock.lang.Shared

import java.time.LocalDateTime
import java.time.Period

class TokensSchedulerIntegrationSpec  extends IntegrationSpecification {
    @Shared
    private static final GenericContainer REDIS_CONTAINER = new GenericContainer("redis:alpine")
            .withExposedPorts(6379)

    private static DBHelper DB_HELPER

    @DynamicPropertySource
    private static void rabbitmqProperties(DynamicPropertyRegistry registry) {
        REDIS_CONTAINER.start()

        registry.add("spring.redis.host", REDIS_CONTAINER::getHost)
        registry.add("spring.redis.port", () -> REDIS_CONTAINER.getMappedPort(6379))
    }

    @Autowired
    private SecurityRepository securityRepository

    @Autowired
    private TokenRepository tokenRepository

    @Autowired
    private TokenService tokenService

    def setup() {
        DB_HELPER = new DBHelper(managerProperties.jwt, securityRepository, tokenRepository)
    }

    def cleanupSpec() {
        REDIS_CONTAINER.stop()
    }

    def cleanup() {
        securityRepository.deleteAll()
        tokenRepository.deleteAll()
    }

    def "delete inactive tokens after period"() {
        given:
        DB_HELPER.insert(SecurityBuilder.new()
                .setPublicId("delete this")
                .setCreationDate(LocalDateTime.now().minusDays(1).minusHours(1))
                .addToken(
                        TokenBuilder.new()
                        .setPublicId("delete this token")
                )
        )
        DB_HELPER.insert(SecurityBuilder.new()
                .setPublicId("do not delete this")
                .addToken(
                        TokenBuilder.new()
                        .setPublicId("do not delete this token")
                )
        )

        when:
        tokenService.deleteInactiveAfterPeriod(Period.ofDays(1))

        then:
        securityRepository.findByPublicId("delete this").isEmpty()
        securityRepository.findByPublicId("do not delete this").isPresent()
        tokenRepository.findAll().findAll().stream()
            .allMatch(token -> token.getPublicId() == "do not delete this token")
    }
}

package net.ignaszak.manager.tokens.integration

import com.nimbusds.jose.JOSEObjectType
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.crypto.factories.DefaultJWSSignerFactory
import com.nimbusds.jose.jwk.JWKSelector
import com.nimbusds.jose.jwk.source.JWKSource
import com.nimbusds.jose.proc.SecurityContext
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import net.ignaszak.manager.commons.test.specification.integration.auth.Authentication
import org.springframework.http.HttpHeaders

import java.sql.Date
import java.time.Instant

import static com.nimbusds.jose.JWSAlgorithm.RS256
import static com.nimbusds.jose.jwk.JWKMatcher.forJWSHeader
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.util.CollectionUtils.isEmpty

class OAuth2Authentication implements Authentication {

    private final HttpHeaders headers

    OAuth2Authentication(JWKSource<SecurityContext> jwkSource, String subject, Set<String> scopes, int expirationSec) {
        def token = generateOAuth2Token(jwkSource, subject, expirationSec, scopes)
        headers = new HttpHeaders()
        headers.setContentType(APPLICATION_JSON)
        headers.add("Authorization", "Bearer ${token}")
    }

    private static String generateOAuth2Token(JWKSource<SecurityContext> jwkSource,
                                              String subject,
                                              int expirationSec,
                                              Set<String> scopes) {
        JWSHeader jwsHeader = new JWSHeader(RS256)
        JWKSelector jwkSelector = new JWKSelector(forJWSHeader(jwsHeader))
        def jwks = jwkSource.get(jwkSelector, null)

        def jws = ""

        if (!isEmpty(jwks)) {
            def jwk = jwks.get(0)

            def headers = new JWSHeader.Builder(RS256)
                    .type(JOSEObjectType.JWT)
                    .keyID(jwk.getKeyID())
                    .build()

            def claims = new JWTClaimsSet.Builder()
                    .subject(subject)
                    .audience(subject)
                    .expirationTime(Date.from(Instant.now().plusSeconds(expirationSec)))
                    .claim("scope", scopes)
                    .build()

            def signedJwt = new SignedJWT(headers, claims)

            def signer = new DefaultJWSSignerFactory().createJWSSigner(jwk)
            signedJwt.sign(signer)

            jws = signedJwt.serialize()
        }

        jws
    }

    @Override
    HttpHeaders getHeaders() {
        headers
    }
}

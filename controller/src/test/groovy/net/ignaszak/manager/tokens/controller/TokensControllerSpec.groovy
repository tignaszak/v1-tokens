package net.ignaszak.manager.tokens.controller

import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import net.ignaszak.manager.tokens.servicecontract.TokenService
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.TestUtils.assertNoContentResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete

@WebMvcTest(TokensController)
class TokensControllerSpec extends ControllerSpecification {

    @SpringBean
    private JwtOAuth2Service jwtOAuth2ServiceMock = Mock()

    @SpringBean
    private TokenService tokenServiceMock = Mock()

    def "should delete current token"() {
        when:
        def request = delete("/api")
                .header(TOKEN_HEADER, anyToken())

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "should delete all tokens"() {
        when:
        def request = delete("/api/all")
                .header(TOKEN_HEADER, anyToken())

        then:
        assertNoContentResponse(mvc.perform(request))
    }
}

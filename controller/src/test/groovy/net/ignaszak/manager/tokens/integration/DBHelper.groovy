package net.ignaszak.manager.tokens.integration

import net.ignaszak.manager.commons.conf.properties.JwtProperties
import net.ignaszak.manager.commons.test.specification.integration.auth.JwtAuthentication
import net.ignaszak.manager.tokens.entity.SecurityEntity
import net.ignaszak.manager.tokens.entity.TokenEntity
import net.ignaszak.manager.tokens.integration.builder.SecurityBuilder
import net.ignaszak.manager.tokens.integration.builder.TokenBuilder
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository

import java.time.LocalDateTime

import static java.util.stream.Collectors.toSet

final class DBHelper {
    public static final LocalDateTime NOW = LocalDateTime.now()

    private final JwtProperties jwtProperties
    private final SecurityRepository securityRepository
    private final TokenRepository tokenRepository

    DBHelper(JwtProperties jwtProperties, SecurityRepository securityRepository, TokenRepository tokenRepository) {
        this.jwtProperties = jwtProperties
        this.securityRepository = securityRepository
        this.tokenRepository = tokenRepository
    }

    String anyUserJwtToken() {
        new JwtAuthentication(jwtProperties.secret, jwtProperties.expirationMs)
                .headers
                .get("Authorization")
                .get(0)
    }

    void insert(Object builder) {
        if (builder instanceof SecurityBuilder) {
            insertSecurity(builder)
        }
    }

    private void insertSecurity(SecurityBuilder securityBuilder) {
        def securityEntity = new SecurityEntity(
                securityBuilder.publicId,
                securityBuilder.userPublicId,
                securityBuilder.creationDate,
                new HashSet<>(),
                securityBuilder.active
        )

        def tokenEntities = securityBuilder.tokens.stream()
                .map(builder -> toTokenEntity(builder, securityEntity.publicId))
                .peek(tokenRepository::save)
                .collect(toSet())

        securityEntity.tokens.addAll(tokenEntities)

        securityRepository.save(securityEntity)
    }

    private TokenEntity toTokenEntity(TokenBuilder tokenBuilder, String securityPublicId) {
        new TokenEntity(
                tokenBuilder.publicId,
                tokenBuilder.jwt == null ? anyUserJwtToken() : tokenBuilder.jwt,
                securityPublicId,
                tokenBuilder.creationDate,
                tokenBuilder.loginDate,
                tokenBuilder.timeToLiveMs
        )
    }
}

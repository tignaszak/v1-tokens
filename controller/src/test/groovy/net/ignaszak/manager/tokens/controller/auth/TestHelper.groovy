package net.ignaszak.manager.tokens.controller.auth

final class TestHelper {

    public static final String OAUTH2_HEADER = "Authorization"

    static final String anyOAuth2() {
        "any oAuth2"
    }

    private TestHelper() {}
}

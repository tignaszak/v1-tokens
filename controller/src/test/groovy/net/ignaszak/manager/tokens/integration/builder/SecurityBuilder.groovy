package net.ignaszak.manager.tokens.integration.builder

import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.tokens.integration.DBHelper

import java.time.LocalDateTime

final class SecurityBuilder {
    static SecurityBuilder "new"() {
        new SecurityBuilder()
    }

    String publicId = UUID.randomUUID().toString()
    String userPublicId = UserHelper.ANY_PUBLIC_ID
    LocalDateTime creationDate = DBHelper.NOW
    Set<TokenBuilder> tokens = new HashSet<>()
    Boolean active = false

    SecurityBuilder setPublicId(final String publicId) {
        this.publicId = publicId
        this
    }

    SecurityBuilder setUserPublicId(final String userPublicId) {
        this.userPublicId = userPublicId
        this
    }

    SecurityBuilder setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate
        this
    }

    SecurityBuilder addToken(final TokenBuilder token) {
        this.tokens.add(token)
        this
    }

    SecurityBuilder setActive(final Boolean active) {
        this.active = active
        this
    }
}

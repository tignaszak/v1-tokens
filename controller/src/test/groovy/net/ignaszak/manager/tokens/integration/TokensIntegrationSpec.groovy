package net.ignaszak.manager.tokens.integration

import com.nimbusds.jose.jwk.source.JWKSource
import com.nimbusds.jose.proc.SecurityContext
import net.ignaszak.manager.commons.rest.token.TokenActivationRequest
import net.ignaszak.manager.commons.rest.token.TokenRegistrationRequest
import net.ignaszak.manager.commons.rest.token.TokenValidationRequest
import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import net.ignaszak.manager.commons.test.specification.integration.RestTemplateFacade
import net.ignaszak.manager.commons.test.specification.integration.RestTemplateFacadeImpl
import net.ignaszak.manager.tokens.integration.builder.SecurityBuilder
import net.ignaszak.manager.tokens.integration.builder.TokenBuilder
import net.ignaszak.manager.tokens.repository.SecurityRepository
import net.ignaszak.manager.tokens.repository.TokenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.GenericContainer
import spock.lang.Shared

import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.tokens.service.error.TokenError.TOK_INVALID_TOKEN
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.FORBIDDEN

class TokensIntegrationSpec extends IntegrationSpecification {

    @Shared
    private static final GenericContainer REDIS_CONTAINER = new GenericContainer("redis:alpine")
            .withExposedPorts(6379)

    private static DBHelper DB_HELPER

    @DynamicPropertySource
    private static void rabbitmqProperties(DynamicPropertyRegistry registry) {
        REDIS_CONTAINER.start()

        registry.add("spring.redis.host", REDIS_CONTAINER::getHost)
        registry.add("spring.redis.port", () -> REDIS_CONTAINER.getMappedPort(6379))
    }

    @Autowired
    private JWKSource<SecurityContext> jwkSource

    @Autowired
    private SecurityRepository securityRepository

    @Autowired
    private TokenRepository tokenRepository

    def setup() {
        DB_HELPER = new DBHelper(managerProperties.jwt, securityRepository, tokenRepository)
    }

    def cleanupSpec() {
        REDIS_CONTAINER.stop()
    }

    def cleanup() {
        securityRepository.deleteAll()
        tokenRepository.deleteAll()
    }

    def "register new token for newly created user"() {
        given:
        def userJwt = DB_HELPER.anyUserJwtToken()
        def request = new TokenRegistrationRequest(ANY_PUBLIC_ID, userJwt)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("register"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/register", request)

        then:
        assertNoContent(responseEntity)

        and:
        def tokenOptional = securityRepository.findByUserPublicId(ANY_PUBLIC_ID)
                .flatMap(s -> s.findTokenByJwt(userJwt))
        tokenOptional.isPresent()
        !tokenOptional.get().jwt.isBlank()
        tokenOptional.get().timeToLiveMs > 0
    }

    def "register new token for existing user"() {
        given:
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .setActive(true)
                        .addToken(TokenBuilder.new())
        )
        def userJwt = DB_HELPER.anyUserJwtToken()
        def request = new TokenRegistrationRequest(ANY_PUBLIC_ID, userJwt)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("register"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/register", request)

        then:
        assertNoContent(responseEntity)

        and:
        securityRepository.findByUserPublicId(ANY_PUBLIC_ID).get().tokens.size() == 2
        securityRepository.findByUserPublicId(ANY_PUBLIC_ID)
                .flatMap(s -> s.findTokenByJwt(userJwt))
                .isPresent()
    }

    def "error while registering token with incorrect jwt"() {
        given:
        def request = new TokenRegistrationRequest(ANY_PUBLIC_ID, "Bearer incorrect")
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("register"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "error while registering token with incorrect user public id"() {
        given:
        def request = new TokenRegistrationRequest("invalid user public id", DB_HELPER.anyUserJwtToken())
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("register"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "error while registering token with incorrect oauth scope"() {
        given:
        def request = new TokenRegistrationRequest(ANY_PUBLIC_ID, DB_HELPER.anyUserJwtToken())
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("invalid"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, AUT_INVALID_TOKEN)
    }

    def "error while registering token with expired oauth"() {
        given:
        def request = new TokenRegistrationRequest(ANY_PUBLIC_ID, DB_HELPER.anyUserJwtToken())
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("register"), -1)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, TOK_INVALID_TOKEN)
    }

    def "error while registering token without OAuth2"() {
        given:
        def request = new TokenRegistrationRequest(ANY_PUBLIC_ID, DB_HELPER.anyUserJwtToken())

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, AUT_INVALID_TOKEN)
    }

    def "validate user jwt token"() {
        given:
        def userJwt = DB_HELPER.anyUserJwtToken()
        def request = new TokenValidationRequest(userJwt)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("validate"), 15)
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .setActive(true)
                        .addToken(
                                TokenBuilder.new()
                                        .setJwt(userJwt)
                                        .setTimeToLiveDays(15)
                        )
        )

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/validate", request)

        then:
        assertNoContent(responseEntity)
        def tokenOptional = securityRepository.findByUserPublicId(ANY_PUBLIC_ID)
            .flatMap(s -> s.findTokenByJwt(userJwt))
        tokenOptional.isPresent()
    }

    def "error while validation non existing token"() {
        given:
        def userJwt = DB_HELPER.anyUserJwtToken()
        def request = new TokenValidationRequest(userJwt)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("validate"), 15)
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .setActive(true)
        )

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/validate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "error while validation non existing token and security entry"() {
        given:
        def userJwt = DB_HELPER.anyUserJwtToken()
        def request = new TokenValidationRequest(userJwt)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("validate"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/validate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "error while validation no existing token"() {
        given:
        def request = new TokenValidationRequest(DB_HELPER.anyUserJwtToken())
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("validate"), 15)
        DB_HELPER.insert(
                SecurityBuilder.new()
        )

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/validate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "error while validation expired token"() {
        given:
        def request = new TokenValidationRequest(DB_HELPER.anyUserJwtToken())
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("validate"), 15)
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .addToken(
                                TokenBuilder.new()
                                        .setTimeToLiveDays(0)
                        )
        )

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/validate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "activate user by jwt token"() {
        given:
        def request = new TokenActivationRequest(ANY_PUBLIC_ID)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("activate"), 15)
        DB_HELPER.insert(SecurityBuilder.new())

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/activate", request)

        then:
        assertNoContent(responseEntity)
        def statusOptional = securityRepository.findByUserPublicId(ANY_PUBLIC_ID)
        statusOptional.isPresent()
        statusOptional.get().active
    }

    def "error while activating not registered user"() {
        given:
        def request = new TokenActivationRequest(ANY_PUBLIC_ID)
        def oauth2RestTemplate = buildOAuth2RestTemplate(Set.of("activate"), 15)

        when:
        def responseEntity = oauth2RestTemplate.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TOK_INVALID_TOKEN)
    }

    def "delete existing token"() {
        setup:
        def userJwt = DB_HELPER.anyUserJwtToken()
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .setActive(true)
                        .addToken(
                                TokenBuilder.new()
                                .setJwt(userJwt)
                        )
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api")

        then:
        assertNoContent(responseEntity)

        and:
        securityRepository.findAll().first().tokens.isEmpty()
        tokenRepository.findByJwt(userJwt).isEmpty()
    }

    def "no error response while deleting no existing token"() {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api")

        then:
        assertNoContent(responseEntity)
    }

    def "delete all user tokens"() {
        setup:
        DB_HELPER.insert(
                SecurityBuilder.new()
                    .addToken(TokenBuilder.new())
        )
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .addToken(TokenBuilder.new())
        )
        DB_HELPER.insert(
                SecurityBuilder.new()
                        .setUserPublicId("other user public id")
                        .addToken(TokenBuilder.new())
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/all")

        then:
        assertNoContent(responseEntity)

        and:
        securityRepository.findByUserPublicId(ANY_PUBLIC_ID).get().tokens.isEmpty()
        securityRepository.findByUserPublicId("other user public id").get().tokens.size() == 1
    }

    def "no error response while deleting no existing tokens"() {
        setup:
        DB_HELPER.insert(
                SecurityBuilder.new()
                    .setUserPublicId("other user public id")
                    .addToken(TokenBuilder.new())
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/all")

        then:
        assertNoContent(responseEntity)

        and:
        securityRepository.findByUserPublicId("other user public id").get().tokens.size() == 1
    }

    private RestTemplateFacade buildOAuth2RestTemplate(Set<String> scopes, int expirationMinutes) {
        new RestTemplateFacadeImpl(
                restTemplate,
                port,
                new OAuth2Authentication(
                        jwkSource,
                        managerProperties.OAuth2.clients["tokens"].id,
                        scopes,
                        60 * expirationMinutes
                )
        )
    }
}

package net.ignaszak.manager.tokens.controller.auth

import com.nimbusds.jose.jwk.source.JWKSource
import com.nimbusds.jose.proc.SecurityContext
import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import net.ignaszak.manager.commons.rest.token.TokenActivationRequest
import net.ignaszak.manager.commons.rest.token.TokenRegistrationRequest
import net.ignaszak.manager.commons.rest.token.TokenValidationRequest
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import net.ignaszak.manager.tokens.servicecontract.TokenAuthService
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType

import java.nio.charset.StandardCharsets

import static net.ignaszak.manager.commons.test.TestUtils.assertNoContentResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static TestHelper.OAUTH2_HEADER
import static net.ignaszak.manager.tokens.controller.auth.TestHelper.anyOAuth2
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@WebMvcTest(AuthController)
class AuthControllerSpec extends ControllerSpecification {

    @SpringBean
    private TokenAuthService tokenAuthServiceMock = Mock()

    @SpringBean
    private JwtOAuth2Service jwtOAuth2ServiceMock = Mock()

    @SpringBean
    private JWKSource<SecurityContext> jwkSourceMock = Mock()

    def "should return TokenResponse if token is valid"() {
        given:
        1 * jwtOAuth2ServiceMock.extractScopes(anyOAuth2()) >> Set.of("validate")
        1 * tokenAuthServiceMock.validate(anyToken())

        when:
        def request = post("/api/auth/validate")
                .header(OAUTH2_HEADER, anyOAuth2())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())
                .content(toJson(anyTokenValidationRequest()))

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "should register new token"() {
        given:
        1 * jwtOAuth2ServiceMock.extractScopes(anyOAuth2()) >> Set.of("register")
        1 * tokenAuthServiceMock.register(anyToken(), UserHelper.ANY_PUBLIC_ID)

        when:
        def request = post("/api/auth/register")
                .header(OAUTH2_HEADER, anyOAuth2())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())
                .content(toJson(anyTokenRegistrationRequest()))

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "should activate tokens"() {
        given:
        1 * jwtOAuth2ServiceMock.extractScopes(anyOAuth2()) >> Set.of("activate")
        1 * tokenAuthServiceMock.activate(UserHelper.ANY_PUBLIC_ID)

        when:
        def request = post("/api/auth/activate")
                .header(OAUTH2_HEADER, anyOAuth2())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())
                .content(toJson(anyTokenActivationRequest()))

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    private static TokenRegistrationRequest anyTokenRegistrationRequest() {
        new TokenRegistrationRequest(UserHelper.ANY_PUBLIC_ID, anyToken())
    }

    private static TokenValidationRequest anyTokenValidationRequest() {
        new TokenValidationRequest(anyToken())
    }

    private static TokenActivationRequest anyTokenActivationRequest() {
        new TokenActivationRequest(UserHelper.ANY_PUBLIC_ID)
    }
}

package net.ignaszak.manager.tokens.integration.builder

import net.ignaszak.manager.tokens.integration.DBHelper

import java.time.LocalDateTime

final class TokenBuilder {

    static TokenBuilder "new" () {
        new TokenBuilder()
    }

    String publicId = UUID.randomUUID().toString()
    String jwt = UUID.randomUUID().toString()
    LocalDateTime creationDate = DBHelper.NOW
    LocalDateTime loginDate = DBHelper.NOW
    Long timeToLiveMs = 86_400_000 * 30

    private TokenBuilder() {}

    TokenBuilder setPublicId(final String publicId) {
        this.publicId = publicId
        this
    }

    TokenBuilder setJwt(final String jwt) {
        this.jwt = jwt
        this
    }

    TokenBuilder setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate
        this
    }

    TokenBuilder setLoginDate(final LocalDateTime loginDate) {
        this.loginDate = loginDate
        this
    }

    TokenBuilder setTimeToLiveDays(final Long timeToLiveDays) {
        this.timeToLiveMs = 86_400_000 * timeToLiveDays
        this
    }
}

package net.ignaszak.manager.tokens.integration

import net.ignaszak.manager.commons.conf.properties.OAuth2Properties
import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap

import static java.lang.Integer.valueOf
import static java.nio.charset.StandardCharsets.UTF_8
import static org.springframework.http.HttpStatus.OK

class OAuth2IntegrationSpec extends IntegrationSpecification {

    def "generate oauth2 token"() {
        given:
        def request = getOAuth2Request(managerProperties.OAuth2.clients.tokens)

        when:
        def responseEntity = restTemplate.postForEntity("http://localhost:${port}/oauth2/token", request, Object.class) as ResponseEntity<Map<String, String>>

        then:
        responseEntity.statusCode == OK
        !responseEntity.body.access_token.isBlank()
        responseEntity.body.token_type == "Bearer"
        valueOf(responseEntity.body.expires_in) > 0
        for (def scope : managerProperties.OAuth2.clients.tokens.scope) {
            assert responseEntity.body.scope.contains(scope)
        }
    }

    private static HttpEntity getOAuth2Request(OAuth2Properties.CredentialProperties properties) {
        def body = new LinkedMultiValueMap<String, String>()
        body.add("grant_type", "client_credentials")
        body.add("scope", properties.scope.join(" "))
        def headers = new HttpHeaders()
        def auth = "${properties.id}:${properties.secret}"
        def encodedAuth = Base64.encoder.encode(auth.getBytes(UTF_8))
        def authHeader = "Basic " + new String(encodedAuth)
        headers.set("Authorization", authHeader)
        new HttpEntity(body, headers)
    }
}

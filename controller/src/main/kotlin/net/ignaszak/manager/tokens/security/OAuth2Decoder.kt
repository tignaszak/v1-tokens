package net.ignaszak.manager.tokens.security

import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.jwk.JWKMatcher
import com.nimbusds.jose.jwk.JWKSelector
import com.nimbusds.jose.jwk.source.JWKSource
import com.nimbusds.jose.proc.SecurityContext
import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import org.apache.logging.log4j.LogManager
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.stereotype.Component
import java.lang.Exception

@Component
class OAuth2Decoder(private val jwkSource: JWKSource<SecurityContext>) : JwtOAuth2Service {

    companion object {
        private val log = LogManager.getLogger(OAuth2Decoder)
    }

    override fun extractScopes(token: String): Set<String> {
        val rawToken = token.replace("Bearer ", "")

        val jwsHeader = JWSHeader(JWSAlgorithm.RS256)
        val jwkSelector = JWKSelector(JWKMatcher.forJWSHeader(jwsHeader))

        val jwkList = jwkSource.get(jwkSelector, null)

        val scopes = mutableSetOf<String>()

        if (jwkList.size == 1) {
            val jwk = jwkList[0]

            try {
                val decoder = NimbusJwtDecoder.withPublicKey(jwk.toRSAKey().toRSAPublicKey()).build()
                val decodedJWK = decoder.decode(rawToken)

                scopes.addAll(decodedJWK.getClaimAsStringList("scope"))
            } catch (e: Exception) {
                log.error("Failed to decode a token", e)

                throw OAuth2Exception()
            }
        } else {
            log.error("Failed to select a JWK signing key")

            throw OAuth2Exception()
        }

        return scopes.toSet()
    }
}
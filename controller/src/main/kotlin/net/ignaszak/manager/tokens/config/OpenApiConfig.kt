package net.ignaszak.manager.tokens.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.security.OAuthFlow
import io.swagger.v3.oas.models.security.OAuthFlows
import io.swagger.v3.oas.models.security.Scopes
import io.swagger.v3.oas.models.security.SecurityScheme
import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.spring.config.CommonOpenApiConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import javax.annotation.PostConstruct

@Configuration
@Import(CommonOpenApiConfig::class)
open class OpenApiConfig {

    @Autowired
    private lateinit var openApi: OpenAPI

    @Autowired
    private lateinit var managerProperties: ManagerProperties

    @PostConstruct
    open fun configureOpenApi() {
        managerProperties.server.gatewayExternalUri?.let {
            openApi.components
                .addSecuritySchemes(
                    "Tokens oAuth2",
                    SecurityScheme()
                        .type(SecurityScheme.Type.OAUTH2)
                        .description("Tokens oAuth2")
                        .flows(
                            OAuthFlows()
                                .clientCredentials(
                                    OAuthFlow()
                                        .tokenUrl(it + managerProperties.oAuth2.tokenEndpoint)
                                        .scopes(
                                            Scopes()
                                                .addString("validate", "Validate user JWT token")
                                                .addString("register", "Register user JWT token")
                                                .addString("activate", "Activate user JWT token")
                                        )
                                )
                        )
                        .`in`(SecurityScheme.In.HEADER)
                        .scheme("Bearer")
                        .bearerFormat("JWT")
                        .name("Authorization")
                )
        }
    }
}
package net.ignaszak.manager.tokens.config

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.spring.config.AuthorizationFilter
import net.ignaszak.manager.commons.spring.config.CommonWebMvcConfig
import net.ignaszak.manager.commons.spring.config.CommonWebSecurityConfig.Companion.applyDefaultSecurity
import net.ignaszak.manager.commons.spring.security.mapper.UserSecurityMapper
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.OAuth2AuthorizationServerConfigurer
import org.springframework.security.oauth2.core.AuthorizationGrantType.CLIENT_CREDENTIALS
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings
import java.util.stream.Collectors

@Configuration
@EnableWebSecurity
@Import(CommonWebMvcConfig::class)
open class AuthorizationServerConfig(
        private val jwtService: JwtService,
        private val jwtOAuth2Service: JwtOAuth2Service,
        private val userSecurityMapper: UserSecurityMapper,
        private val uriMatcher: UriMatcher
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        val filter = AuthorizationFilter(
                authenticationManager() ?: authenticationManagerBean(),
                userSecurityMapper,
                jwtService,
                jwtOAuth2Service,
                uriMatcher
        )

        applyOauth2Security(http)
        applyDefaultSecurity(http, filter)
    }

    private fun applyOauth2Security(http: HttpSecurity) {
        val configurer = TokensOAuth2AuthorizationServerConfigurer(
                OAuth2AuthorizationServerConfigurer()
        )

        http.apply(configurer)
    }

    @Bean
    open fun providerSettings(): ProviderSettings = ProviderSettings.builder().build()

    @Bean
    open fun registeredClientRepository(managerProperties: ManagerProperties): RegisteredClientRepository {
        val clientList = managerProperties.oAuth2.clients.entries
                .stream()
                .map {
                    val builder = RegisteredClient.withId(it.key)
                            .clientId(it.value.id)
                            .clientSecret("{noop}" + it.value.secret)
                            .authorizationGrantType(CLIENT_CREDENTIALS)

                    it.value.scope.forEach(builder::scope)

                    return@map builder.build()
                }
                .collect(Collectors.toList())

        return InMemoryRegisteredClientRepository(clientList)
    }
}
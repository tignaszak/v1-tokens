package net.ignaszak.manager.tokens

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
@OpenAPIDefinition(info = Info(title = "Tokens API", version = "1.0"))
open class TokensApplication

fun main(args: Array<String>) {
    runApplication<TokensApplication>(*args)
}
package net.ignaszak.manager.tokens.controller.auth

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import net.ignaszak.manager.commons.openapi.annotations.ApiForbiddenResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse
import net.ignaszak.manager.commons.rest.token.TokenActivationRequest
import net.ignaszak.manager.commons.rest.token.TokenRegistrationRequest
import net.ignaszak.manager.commons.rest.token.TokenValidationRequest
import net.ignaszak.manager.commons.spring.handler.annotation.InnerMapping
import net.ignaszak.manager.tokens.servicecontract.TokenAuthService
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = "Tokens auth", description = "Token authorization operations")
class AuthController(private val tokenAuthService: TokenAuthService) {

    @PostMapping("/auth/validate")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Validate user jwt")
    @InnerMapping("validate")
    @ApiNoContentResponse
    @ApiForbiddenResponse
    @SecurityRequirement(name = "Tokens oAuth2")
    fun validate(@RequestBody request: TokenValidationRequest) {
        tokenAuthService.validate(request.jwt)
    }

    @PostMapping("/auth/register")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Register user jwt")
    @InnerMapping("register")
    @ApiNoContentResponse
    @ApiForbiddenResponse
    @SecurityRequirement(name = "Tokens oAuth2")
    fun register(@RequestBody request: TokenRegistrationRequest) {
        tokenAuthService.register(request.jwt, request.userPublicId)
    }

    @PostMapping("/auth/activate")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Activate user jwt")
    @InnerMapping("activate")
    @ApiNoContentResponse
    @ApiForbiddenResponse
    @SecurityRequirement(name = "Tokens oAuth2")
    fun activate(@RequestBody request: TokenActivationRequest) {
        tokenAuthService.activate(request.userPublicId)
    }
}
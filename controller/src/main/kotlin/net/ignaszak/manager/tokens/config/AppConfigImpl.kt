package net.ignaszak.manager.tokens.config

import org.springframework.stereotype.Component
import java.time.Period

@Component
class AppConfigImpl : AppConfig {
    companion object {
        private const val CRON_EVERY_HOUR = "0 0 0/1 1/1 * ?"
        private val DELETE_INACTIVE_USER_PERIOD = Period.ofDays(1)
    }

    override fun getDeleteInactiveUserPeriod(): Period = DELETE_INACTIVE_USER_PERIOD

    override fun getDeleteInactiveUserCronSchedule() = CRON_EVERY_HOUR
}
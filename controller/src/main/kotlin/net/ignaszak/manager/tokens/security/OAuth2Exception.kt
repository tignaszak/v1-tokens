package net.ignaszak.manager.tokens.security

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.tokens.service.error.TokenError.TOK_INVALID_TOKEN
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(FORBIDDEN)
class OAuth2Exception : ErrorException(TOK_INVALID_TOKEN)
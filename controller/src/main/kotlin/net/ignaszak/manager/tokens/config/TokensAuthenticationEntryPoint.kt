package net.ignaszak.manager.tokens.config

import net.ignaszak.manager.commons.error.AuthError
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.config.WebConfigHelper.setResponse
import org.springframework.beans.factory.InitializingBean
import org.springframework.http.HttpStatus
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TokensAuthenticationEntryPoint : AuthenticationEntryPoint, InitializingBean {

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, authException: AuthenticationException) {
        setResponse(
                response,
                HttpStatus.FORBIDDEN,
                ErrorResponse.of(AuthError.AUT_AUTHENTICATION.code, AuthError.AUT_AUTHENTICATION.message)
        )
    }

    override fun afterPropertiesSet() {
        // do nothing
    }
}
package net.ignaszak.manager.tokens.config

import java.time.Period

interface AppConfig {
    fun getDeleteInactiveUserPeriod(): Period
    fun getDeleteInactiveUserCronSchedule(): String
}
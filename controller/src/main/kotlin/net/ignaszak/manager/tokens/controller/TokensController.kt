package net.ignaszak.manager.tokens.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement
import net.ignaszak.manager.commons.user.annotation.UserPrincipal
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import net.ignaszak.manager.tokens.servicecontract.TokenService
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = "Token operations", description = "Token operations")
class TokensController(private val tokenService: TokenService) {

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Delete current token")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun deleteCurrent(
        @Parameter(hidden = true) @UserPrincipal userPrincipalTO: UserPrincipalTO,
        @Parameter(hidden = true) @RequestHeader("Authorization") jwt: String
    ) = tokenService.delete(userPrincipalTO.publicId, jwt)

    @DeleteMapping("/all")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Delete all tokens")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun deleteAll(@UserPrincipal userPrincipalTO: UserPrincipalTO) = tokenService.deleteAll(userPrincipalTO.publicId)
}
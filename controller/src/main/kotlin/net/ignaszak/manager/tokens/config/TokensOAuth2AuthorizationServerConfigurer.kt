package net.ignaszak.manager.tokens.config

import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.OAuth2AuthorizationServerConfigurer

class TokensOAuth2AuthorizationServerConfigurer(
        private val configurer: AbstractHttpConfigurer<OAuth2AuthorizationServerConfigurer<HttpSecurity>, HttpSecurity>
) : AbstractHttpConfigurer<OAuth2AuthorizationServerConfigurer<HttpSecurity>, HttpSecurity>() {

    override fun init(builder: HttpSecurity) {
        configurer.init(builder)

        val exceptionHandling = builder.exceptionHandling()
        val authenticationEntryPoint = TokensAuthenticationEntryPoint()

        exceptionHandling.authenticationEntryPoint(authenticationEntryPoint)
    }

    override fun configure(builder: HttpSecurity) {
        configurer.configure(builder)
    }
}
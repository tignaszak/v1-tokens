package net.ignaszak.manager.tokens.scheduler

import net.ignaszak.manager.tokens.config.AppConfig
import net.ignaszak.manager.tokens.servicecontract.TokenService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class TokenScheduler(private val tokenService: TokenService, private val appConfig: AppConfig) {
    @Scheduled(cron = "#{appConfigImpl.deleteInactiveUserCronSchedule}")
    fun removeInactiveTokens() = tokenService.deleteInactiveAfterPeriod(appConfig.getDeleteInactiveUserPeriod())
}
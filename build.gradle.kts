import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    dependencies {
        classpath(Conf.Deps.KOTLIN_NOARG)
    }
}

plugins {
    id("org.springframework.boot") version Conf.Versions.SPRING_BOOT
    id("io.spring.dependency-management") version Conf.Versions.DEPENDENCY_MANAGEMENT

    kotlin("jvm") version Conf.Versions.KOTLIN
    kotlin("plugin.spring") version Conf.Versions.KOTLIN
    kotlin("kapt") version Conf.Versions.KOTLIN
}

java.sourceCompatibility = JavaVersion.VERSION_18
java.targetCompatibility = JavaVersion.VERSION_18

springBoot {
    mainClass.set("net.ignaszak.manager.tokens.TokensApplicationKt")
}

allprojects {
    group = Conf.GROUP
    version = Conf.VERSION

    repositories {
        mavenCentral()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=compatibility")
            jvmTarget = Conf.Versions.JAVA
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()

        testLogging {
            events("passed", "skipped", "failed")
            exceptionFormat = FULL
            showCauses = true
            showExceptions = true
            showStackTraces = true
        }

        afterSuite(KotlinClosure2<TestDescriptor, TestResult, Unit>({desc, result ->
            if (desc.parent != null) {
                val output = result.run {
                    "${desc.name} results: $resultType ($testCount tests, $successfulTestCount successes, " +
                            "$failedTestCount failures, $skippedTestCount skipped)"
                }
                println(output)
            }
            Unit
        }))
    }
}

subprojects {
    repositories {
        mavenCentral()
        maven {
            setUrl("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }

    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "java")
    apply(plugin = "io.spring.dependency-management")
    apply(plugin = "groovy")
    apply(plugin = "kotlin-kapt")

    allProjectDeps()
}

project(":controller") {
    tasks.jar {
        enabled = false
    }

    dependencies {
        apply(plugin = "org.springframework.boot")

        api(project(":rest"))
        api(project(":service"))
        api(project(":servicecontract"))
        api(project(":to"))

        implementation(Conf.Deps.MANAGER_COMMONS_CONF)
        implementation(Conf.Deps.MANAGER_COMMONS_ERROR)
        implementation(Conf.Deps.MANAGER_COMMONS_JWT)
        implementation(Conf.Deps.MANAGER_COMMONS_OPENAPI)
        implementation(Conf.Deps.MANAGER_COMMONS_REST)
        implementation(Conf.Deps.MANAGER_COMMONS_SPRING)
        implementation(Conf.Deps.MANAGER_COMMONS_USER)

        implementation(Conf.Deps.JACKSON)
        implementation(Conf.Deps.JAXB_API)
        implementation(Conf.Deps.JJWT_API)
        implementation(Conf.Deps.JJWT_IMPL)
        implementation(Conf.Deps.JJWT_JACSON)
        implementation(Conf.Deps.OPENAPI)
        implementation(Conf.Deps.OPENAPI_KOTLIN)
        implementation(Conf.Deps.SPRING_BOOT_STARTER_AMQP)
        implementation(Conf.Deps.SPRING_BOOT_STARTER_SECURITY)
        implementation(Conf.Deps.SPRING_BOOT_STARTER_WEB)
        implementation(Conf.Deps.SPRING_CLOUD_CONFIG_BOOTSTRAP)
        implementation(Conf.Deps.SPRING_CLOUD_STARTER_CONFIG)
        implementation(Conf.Deps.SPRING_CLOUD_STARTER_NETFLIX_EUREKA_CLIENT)
        implementation(Conf.Deps.SPRING_SECURITY_OAUTH2)
        implementation(Conf.Deps.SPRING_SECURITY_OAUTH2_AUTH_SERVER)

        testImplementation(Conf.Deps.GSON)
        testImplementation(Conf.Deps.MANAGER_COMMONS_TEST)
        testImplementation(Conf.Deps.MANAGER_COMMONS_UTILS)
        testImplementation(Conf.Deps.SPOCK_SPRING)
        testImplementation(Conf.Deps.SPRING_BOOT_STARTER_DATA_REDIS)
        testImplementation(Conf.Deps.SPRING_BOOT_STARTER_TEST)
        testImplementation(Conf.Deps.SPRING_SECURITY_TEST)
        testImplementation(Conf.Deps.TESTCONTAINERS)
        testImplementation(project(":entity"))
        testImplementation(project(":repository"))
    }
}

project(":rest") {
    dependencies {
        implementation(Conf.Deps.SWAGGER_ANNOTATIONS)
    }
}

project(":to") {
}

project(":repository") {
    dependencies {
        api(project(":entity"))

        implementation(Conf.Deps.SPRING_BOOT_STARTER_DATA_REDIS)

    }
}

project(":entity") {
    dependencies {
        implementation(Conf.Deps.SPRING_BOOT_STARTER_DATA_REDIS)
    }
}

project(":service") {
    dependencies {
        api(project(":entity"))
        api(project(":repository"))
        api(project(":servicecontract"))
        api(project(":to"))

        implementation(Conf.Deps.MANAGER_COMMONS_ERROR)
        implementation(Conf.Deps.MANAGER_COMMONS_JWT)

        implementation(Conf.Deps.SPRING_CONTEXT)
        implementation(Conf.Deps.SPRING_DATA_COMMONS)
        implementation(Conf.Deps.SPRING_WEB) {
            exclude(group = "org.springframework", module = "spring-core")
            exclude(group = "org.springframework", module = "spring-beans")
        }

        implementation(Conf.Deps.JAXB_API)
        implementation(Conf.Deps.LOG4J)

        testImplementation(Conf.Deps.MANAGER_COMMONS_TEST)
        testImplementation(Conf.Deps.MANAGER_COMMONS_USER)
    }
}

project(":servicecontract") {
    dependencies {
        api(project(":to"))
    }
}
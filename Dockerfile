FROM openjdk:19-alpine
ADD controller/build/libs/*.jar manager_tokens_v1_app.jar
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/manager_tokens_v1_app.jar"]
EXPOSE 8080
EXPOSE 5005
import org.gradle.api.Project
import org.gradle.kotlin.dsl.*

object Conf {

    val LOCAL_MAVEN_REPOSITORY = "maven-repository/"

    const val GROUP = "net.ignaszak"
    const val VERSION = "1.0.0-SNAPSHOT"

    object Versions {
        const val MANAGER_COMMONS = "master-SNAPSHOT"

        const val KOTLIN = "1.7.20"
        const val JAVA = "18"

        const val SPRING_AUTHORIZATION_SERVER = "0.3.1"
        const val SPRING_BOOT = "2.7.5"
        const val SPRING_BOOTSTRAP = "3.1.4"
        const val SPRING_CLOUD = "3.1.4"
        const val SPRING_DATA_COMMONS = "2.7.5"
        const val SPRING_EUREKA = "3.1.4"
        const val SPRING_FRAMEWORK = "5.3.23"
        const val SPRING_SECURITY = "5.7.5"
        const val SPRING_SECURITY_OAUTH2 = "2.5.2.RELEASE"

        const val DEPENDENCY_MANAGEMENT = "1.1.0"
        const val GROOVY = "4.0.6"
        const val GSON = "2.10"
        const val JACKSON_KOTLIN = "2.13.4"
        const val JAXB = "2.3.1"
        const val JJWT = "0.11.5"
        const val LOG4J = "2.19.0"
        const val MAPSTRUCT = "1.5.3.Final"
        const val OPENAPI = "1.6.12"
        const val SNAKEYAML = "1.33"
        const val SPOCK = "2.3-groovy-4.0"
        const val SWAGGER_ANNOTATIONS = "2.2.6"
        const val TESTCONTAINERS = "1.17.5"
    }

    object Deps {
        // Kotlin
        const val KOTLIN_REFLECT = "org.jetbrains.kotlin:kotlin-reflect"
        const val KOTLIN_STDLIN_JDK8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
        const val STDLIB = "stdlib"
        // Spock
        const val GROOVY = "org.apache.groovy:groovy:${Versions.GROOVY}"
        const val SPOCK_CORE = "org.spockframework:spock-core:${Versions.SPOCK}"
        const val SPOCK_SPRING = "org.spockframework:spock-spring:${Versions.SPOCK}"
        // Spring
        const val SPRING_BOOT_STARTER_AMQP = "org.springframework.boot:spring-boot-starter-amqp:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_DATA_REDIS = "org.springframework.boot:spring-boot-starter-data-redis:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_SECURITY = "org.springframework.boot:spring-boot-starter-security:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_TEST = "org.springframework.boot:spring-boot-starter-test:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_WEB = "org.springframework.boot:spring-boot-starter-web:${Versions.SPRING_BOOT}"
        const val SPRING_CLOUD_CONFIG_BOOTSTRAP = "org.springframework.cloud:spring-cloud-starter-bootstrap:${Versions.SPRING_BOOTSTRAP}"
        const val SPRING_CLOUD_STARTER_CONFIG = "org.springframework.cloud:spring-cloud-starter-config:${Versions.SPRING_CLOUD}"
        const val SPRING_CLOUD_STARTER_NETFLIX_EUREKA_CLIENT = "org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:${Versions.SPRING_EUREKA}"
        const val SPRING_CONTEXT = "org.springframework:spring-context:${Versions.SPRING_FRAMEWORK}"
        const val SPRING_DATA_COMMONS = "org.springframework.data:spring-data-commons:${Versions.SPRING_DATA_COMMONS}"
        const val SPRING_SECURITY_OAUTH2 = "org.springframework.security.oauth:spring-security-oauth2:${Versions.SPRING_SECURITY_OAUTH2}"
        const val SPRING_SECURITY_OAUTH2_AUTH_SERVER = "org.springframework.security:spring-security-oauth2-authorization-server:${Versions.SPRING_AUTHORIZATION_SERVER}"
        const val SPRING_SECURITY_TEST = "org.springframework.security:spring-security-test:${Versions.SPRING_SECURITY}"
        const val SPRING_WEB = "org.springframework:spring-web:${Versions.SPRING_FRAMEWORK}"
        // Commons
        const val MANAGER_COMMONS_CONF = "net.ignaszak:manager-commons-conf:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_ERROR = "net.ignaszak:manager-commons-error:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_JWT = "net.ignaszak:manager-commons-jwt:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_OPENAPI = "net.ignaszak:manager-commons-openapi:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_REST = "net.ignaszak:manager-commons-rest:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_SPRING = "net.ignaszak:manager-commons-spring:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_TEST = "net.ignaszak:manager-commons-test:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_USER = "net.ignaszak:manager-commons-user:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_UTILS = "net.ignaszak:manager-commons-utils:${Versions.MANAGER_COMMONS}"
        // JJWT
        const val JJWT_API = "io.jsonwebtoken:jjwt-api:${Versions.JJWT}"
        const val JJWT_IMPL = "io.jsonwebtoken:jjwt-impl:${Versions.JJWT}"
        const val JJWT_JACSON = "io.jsonwebtoken:jjwt-jackson:${Versions.JJWT}"
        // Others
        const val GSON = "com.google.code.gson:gson:${Versions.GSON}"
        const val JACKSON = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.JACKSON_KOTLIN}"
        const val JAXB_API = "javax.xml.bind:jaxb-api:${Versions.JAXB}"
        const val KOTLIN_NOARG = "org.jetbrains.kotlin:kotlin-noarg:${Versions.KOTLIN}"
        const val LOG4J = "org.apache.logging.log4j:log4j-api:${Versions.LOG4J}"
        const val MAPSTRUCT = "org.mapstruct:mapstruct:${Versions.MAPSTRUCT}"
        const val MAPSTRUCT_PROCESSOR = "org.mapstruct:mapstruct-processor:${Versions.MAPSTRUCT}"
        const val OPENAPI = "org.springdoc:springdoc-openapi-webmvc-core:${Versions.OPENAPI}"
        const val OPENAPI_KOTLIN = "org.springdoc:springdoc-openapi-kotlin:${Versions.OPENAPI}"
        const val SNAKEYAML = "org.yaml:snakeyaml:${Versions.SNAKEYAML}"
        const val SWAGGER_ANNOTATIONS = "io.swagger.core.v3:swagger-annotations:${Versions.SWAGGER_ANNOTATIONS}"
        const val TESTCONTAINERS = "org.testcontainers:testcontainers:${Versions.TESTCONTAINERS}"
    }
}

fun Project.allProjectDeps(){
    dependencies {
        "implementation" (Conf.Deps.KOTLIN_REFLECT)
        "implementation" (Conf.Deps.KOTLIN_STDLIN_JDK8)
        "implementation" (Conf.Deps.MAPSTRUCT)
        "implementation" (Conf.Deps.SNAKEYAML)
        "implementation" (kotlin(Conf.Deps.STDLIB))
        "implementation" (Conf.Deps.MANAGER_COMMONS_UTILS)
        "kapt" (Conf.Deps.MAPSTRUCT_PROCESSOR)
        "testImplementation" (Conf.Deps.SPOCK_CORE)
        "testImplementation" (Conf.Deps.GROOVY)
    }
}
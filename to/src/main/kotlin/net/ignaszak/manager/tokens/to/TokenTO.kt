package net.ignaszak.manager.tokens.to

import java.time.LocalDateTime

data class TokenTO(
        val publicId: String,
        val jwt: String,
        val userPublicId: String,
        val creationDate: LocalDateTime,
        val loginDate: LocalDateTime,
        val timeToLiveMs: Long
)
